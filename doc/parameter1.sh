#!/bin/bash
# Author: Heinz Mielimonka
# Version: @(#).parameter.sh 1.0.0 10.03.2023 (c)2023 Heinz Mielimonka
#
#@  Set needed parameter to run healthcheck.storage.sh
#@
#
# Update history:
#
# V 1.0.0 10.03.2023 initial version
#



# ---------------------------------------------------------------------------------------------------------------------------------------------
# CUSTOMER SPECIFIC VALUES - please update appropriate
# ---------------------------------------------------------------------------------------------------------------------------------------------
export COMPARTMENT_OCID=[please insert your value here]																	# Compartment Name: 
export REGION_PROFILE=FRANKFURT																							# oci region profile e.g. frankfurt
export CREATE_BLOCK_VOLUME=0																							# create Block volume 0=no // 1=yes
export CREATE_BLOCK_VOLUME_BACKUP=0																						# create Block volume backup 0=no // 1=yes

# ---------------------------------------------------------------------------------------------------------------------------------------------
# SECURITY SPECIFIC VALUES - please update appropriate
# ---------------------------------------------------------------------------------------------------------------------------------------------
export AD=1																												#
export AD_PREFIX=fyxu																									# Prifix of Availability Domain
export FRANKFURT_REGION_IDENTIFIER=eu-frankfurt-1																		# 
export FRANKFURT_BLOCK_VOLUME_NAME=BlockVolumeFrankfurt																	#
export FRANKFURT_AVAILABILITY_DOMAIN="${AD_PREFIX}:${FRANKFURT_REGION_IDENTIFIER}-AD-${AD}"								# fyxu:[Region Identifier]-AD-[Number of Availability Domain] see https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm
export VAULT_OCID=[please insert your value here]																		# Vault OCID
export MasterEncryptionKey_OCID=[please insert your value here]															# MasterEncryptionKey OCID

# ---------------------------------------------------------------------------------------------------------------------------------------------
# VALUES (normally do not touch)
# ---------------------------------------------------------------------------------------------------------------------------------------------
export LOG_FILE="/home/opc/storage.health.check.log"                            										# 
export DEBUG_LEVEL=0                                                													# 0 (off); 1 (low); 2 (high)
export PF1=###																											# Prefix 1 - used to introduce function
export PF2="${PF1}.###:"																								# Prefix 2 - used to log informations inside functions
export MYcolor="${IYellow}"                                         													# define output color
if [ ${DEBUG_LEVEL} -eq 0 ] ; then export MYcolor=$ICyan   ; fi
if [ ${DEBUG_LEVEL} -eq 1 ] ; then export MYcolor=$IPurple ; fi
if [ ${DEBUG_LEVEL} -eq 2 ] ; then export MYcolor=$IRed    ; fi
# ---------------------------------------------------------------------------------------------------------------------------------------------
# end of file
# ---------------------------------------------------------------------------------------------------------------------------------------------
