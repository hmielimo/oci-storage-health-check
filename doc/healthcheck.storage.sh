#!/bin/bash
# Author: Heinz Mielimonka
# Version: @(#).healthcheck.storage.sh 1.0.0 10.03.2023 (c)2023 Heinz Mielimonka
#
#@  healthcheck.storage.sh
#@
#
# Update history:
#
# V 1.0.0 10.03.2023 initial version
#

# ---------------------------------------------------------------------------------------------------------------------------------------------
# prepare environement (load functions)
# ---------------------------------------------------------------------------------------------------------------------------------------------

source functions0.sh

# ---------------------------------------------------------------------------------------------------------------------------------------------
# Oracle Cloud Infrastructure CLI Command Reference - https://docs.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/
# ---------------------------------------------------------------------------------------------------------------------------------------------

MYOUTPUT="Healthcheck Storage" && MYCOUNT=$((1)) 
if [ 1 -eq 1 ] ; then
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"

echo "TYP;Volume Name;Volume OCID;Backup Policy Name; Backup Policy OCID" > "${LOG_FILE}"

PREFIX="### "
PREFIX2="###.### "

color_print "${MYcolor}" "${PREFIX} get BOOT volume list"
if [ 1 -eq 1 ] ; then # get BOOT volume list
  tempfile myTEMPFILE
  oci --profile "${REGION_PROFILE}" bv boot-volume list --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myOCID"
  
    tempfile myTEMPFILE1
    oci --profile "${REGION_PROFILE}" bv volume-backup-policy-assignment get-volume-backup-policy-asset-assignment --asset-id "${myOCID}" > "${myTEMPFILE1}"
    if [[ -s $myTEMPFILE1 ]] ; then
      for ii in $(cat "${myTEMPFILE1}"|jq --raw-output '.[] | .[]."policy-id" ')
      do
        myBACKUPPOLICYOCID=$ii
      done  
	  tempfile myTEMPFILE2
      oci --profile "${REGION_PROFILE}" bv volume-backup-policy get --policy-id "${myBACKUPPOLICYOCID}" > "${myTEMPFILE2}"
      myBACKUPPOLICYNAME=$( cat "${myTEMPFILE2}" | grep display-name | awk '{ print $2 }' )
      echo "BOOT Volume; $myNAME; $myOCID; $myBACKUPPOLICYNAME; $myBACKUPPOLICYOCID" >> "${LOG_FILE}"
    else
      echo "BOOT Volume; $myNAME; $myOCID; ; " >> "${LOG_FILE}"
    fi
  done
fi

if [ 1 -eq 1 ] ; then # get BLOCK volume list
  tempfile myTEMPFILE
  oci --profile "${REGION_PROFILE}" bv volume list --compartment-id "${COMPARTMENT_OCID}" > "${myTEMPFILE}"
  for ii in $(cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(."lifecycle-state"=="AVAILABLE")] | .[]."id" ')
  do
    myOCID=$ii
    myNAME=$( cat "${myTEMPFILE}"|jq --raw-output '.[] | [.[] | select(.'\"id\"'=='\"$ii\"')] | .[].'\"display-name\"' ' )
  
    color_print "${MYcolor}" "${PREFIX2} $myNAME, $myOCID"
  
    tempfile myTEMPFILE1
    oci --profile "${REGION_PROFILE}" bv volume-backup-policy-assignment get-volume-backup-policy-asset-assignment --asset-id "${myOCID}" > "${myTEMPFILE1}"
    if [[ -s $myTEMPFILE1 ]] ; then
      for ii in $(cat "${myTEMPFILE1}"|jq --raw-output '.[] | .[]."policy-id" ')
      do
        myBACKUPPOLICYOCID=$ii
      done  
	  tempfile myTEMPFILE2
      oci --profile "${REGION_PROFILE}" bv volume-backup-policy get --policy-id "${myBACKUPPOLICYOCID}" > "${myTEMPFILE2}"
      myBACKUPPOLICYNAME=$( cat "${myTEMPFILE2}" | grep display-name | awk '{ print $2 }' )
      echo "BLOCK Volume; $myNAME; $myOCID; $myBACKUPPOLICYNAME; $myBACKUPPOLICYOCID" >> "${LOG_FILE}"
    else
      echo "BLOCK Volume; $myNAME; $myOCID; ; " >> "${LOG_FILE}"
    fi
  done
fi

fi

MYOUTPUT="End of Programm" && MYCOUNT=$(($MYCOUNT + 1)) 
color_print "${IGreen}" "($MYCOUNT) $(date "+%d.%m.%Y %H:%M:%S") : $MYOUTPUT"
# ---------------------------------------------------------------------------------------------------------------------------------------------
# end of file
# ---------------------------------------------------------------------------------------------------------------------------------------------