# Oracle Cloud Infrastructure (OCI) Storage Health-Check

[back to Next-Generation Cloud][home]

## Table of Contents
<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:0 orderedList:0 -->

- [Table of Contents](#table-of-contents)
- [Introduction](#introduction)
- [Set up your operation Server](#set-up-your-operation-server)
- [Do Storage Health-Check](#do-storage-health-check)
- [Secure your Storage](#secure-your-storage)
- [check OCI File Storage Service consumption](#check-oci-file-storage-service-consumption)
- [Summary](#summary)

<!-- /TOC -->

## Introduction

This OCI Storage Health-Check script will find all boot and block volumes (not terminated) within a region and compartment (specified by you) and list them including their backup policy if given. So you are able to find easily all your boot and block volumes without using a backup policy to automate backups.

This address perfectly [Best practices framework for Oracle Cloud Infrastructure](https://docs.oracle.com/en/solutions/oci-best-practices/index.html) in general and [Backup Data in Storage Services - Oracle Cloud Infrastructure Block Volumes](https://docs.oracle.com/en/solutions/oci-best-practices/back-your-data1.html) in particular.

You might also have a look at [Monitor OCI Compute Instances and manage VM disk utilization using OCI Stack Monitoring service](https://blogs.oracle.com/cloud-infrastructure/post/monitor-and-manage-using-oci-stack-monitoring) and our [step-by-step tutorial to learn how to manage disk utilization using Stack Monitoring](https://docs.oracle.com/en/learn/stack-monitoring-vm/#stack-monitoring-features).

>>>
Management and monitoring are the backbones of any IT infrastructure environment, without which no one can track the issues, changes, or updates done on the environment. Many tools and services are available in the market for this purpose, but deploying and integrating them in your cloud or on-premises environment can be a hectic task and requires effort and expertise to manage it. To reduce these efforts, Oracle Cloud Infrastructure (OCI) offers many tools under its [Observability and Management Portfolio](https://docs.oracle.com/en-us/iaas/Content/cloud-adoption-framework/monitoring-visibility-audit.htm), including [Stack Monitoring](https://docs.oracle.com/en-us/iaas/stack-monitoring/index.html).

Stack Monitoring provides metrics for availability, CPU, Memory, disk activity, and paging of the Compute instances. It also allows virtual machine (VM) disk utilization monitoring and the tools to monitor file systems created on each disk attached with a Compute instance.
>>>

[Demystifying Logging and Monitoring Agent Types in OCI Observability and Management](https://blogs.oracle.com/observability/post/la-demystifying-agent-om-oci) go more into the Oracle Cloud Infrastructure Observability & Management Reference Architecture incl. Block Storage.

<img alt="Oracle Cloud Infrastructure Observability & Management Reference Architecture" src="doc/images/Observability.and.Management.Reference.Architecture.png" title="Oracle Cloud Infrastructure Observability & Management Reference Architecture" width="100%">

In addition you might make use of [Storage Healthcheck list](https://gitlab.com/hmielimo/oci-storage-health-check/-/blob/main/doc/healthcheck.storage.xlsx) where we list all storage related OCI best practices of the following areas:
- Security and compliance
- Reliability and resilience
- Performance and cost optimization
- Operational efficiency


## Set up your operation Server

### Prerequisite
Please have the following resources ready before starting the script:

- OCI user
- OCI compartment
- OCI region access
- optional: use [How to create a new operate server](https://gitlab.com/hmielimo/next-generation-cloud/-/tree/main/doc/how.to.create.a.new.operate.server)
- A set of keys (please use the [SSH Key Generator](https://standby.cloud/ssh-keygen/) for your convenience)
- An oracle enterprise linux 8 server with [Command Line Interface (CLI)](https://docs.oracle.com/en-us/iaas/Content/API/Concepts/cliconcepts.htm)
- copy the software ([parameter1.sh](https://gitlab.com/hmielimo/oci-storage-health-check/-/blob/main/doc/parameter1.sh), [functions0.sh](https://gitlab.com/hmielimo/oci-storage-health-check/-/blob/main/doc/functions0.sh) and [healthcheck.storage.sh](https://gitlab.com/hmielimo/oci-storage-health-check/-/blob/main/doc/healthcheck.storage.sh)) to the oracle enterprise linux 8 server (/home/opc/)
- update parameter1.sh with your values

Please be aware that the script collect and provide only OCI resources based on the user permissions only within compartment and region you set in parameter1.sh.

## Do Storage Health-Check

~~~
#
# Start your OCI Storage Health-Check within a Region and Compartment
#
/home/opc/healthcheck.storage.sh
~~~
open the LOG_FILE with Excel (it's a CSV formatted file) and do further quality checks on your data.

Here you find an example screenshot

<img alt="example screenshot of a OCI STorage Health-Check" src="doc/images/storage.health.check.1.png" title="example screenshot of a OCI STorage Health-Check" width="80%">

Here we have only backup policy in place for database servers (boot volume and data volume) in production and development but no backup policy for application servers.

In addition you might find the demonstration script [OCI-ShowBackups](https://github.com/Olygo/OCI-ShowBackups/tree/main) showing you how to list compute instance backups (*boot & block volumes*) also valuable.

## Secure your Storage

In [Best practices framework for Oracle Cloud Infrastructure](https://docs.oracle.com/en/solutions/oci-best-practices/index.html) we suggest to implement the following:

- [Encrypt Data in Block Volumes with custom managed keys](https://docs.oracle.com/en/solutions/oci-best-practices/protect-data-rest1.html)
- [Backup Data in Storage Services](https://docs.oracle.com/en/solutions/oci-best-practices/back-your-data1.html)
- [Periodically rotating keys](https://docs.oracle.com/en-us/iaas/Content/KeyManagement/Concepts/keyoverview.htm#concepts)

To do so easily I provide you [secure.storage.sh](https://gitlab.com/hmielimo/oci-storage-health-check/-/blob/main/doc/secure.storage.sh) as a reference. This will do the following:

- create a 50 GB block volume and a backup (using Oracle managed Keys)
- Secure Storage (block volume and backup) with Customer managed keys
- Rotation of Customer managed Key
- Secure Storage (block volume and backup) with Oracle managed keys

Feel free to adapt to your needs.

## check OCI File Storage Service consumption

[File System Usage and Metering](https://docs.oracle.com/en-us/iaas/Content/File/Concepts/FSutilization.htm#File_System_Usage_and_Metering) 
describes how usage and metering are calculated for your file systems, to help you understand and manage your service costs. 
This topic also describes different ways to see your file system, clone, and snapshot utilization and the differences in reporting that can occur depending on which method you use.


Space Allocation

The File Storage service allocates space in blocks of variable size in a way that minimizes total customer cost and optimizes performance. 
Other storage systems might allocate blocks differently than Oracle Cloud Infrastructure File Storage. 
If you copy files from another storage device to your Oracle Cloud Infrastructure file system, you might see minor differences when you compare the physical file size before and after copying.

[File System Metered Utilization](https://docs.oracle.com/en-us/iaas/Content/File/Concepts/FSutilization.htm#Metering_and_Service_Cost__Metered)
reflect the following areas:
- [Snapshot Metered Utilization](https://docs.oracle.com/en-us/iaas/Content/File/Concepts/FSutilization.htm#Snapshot_Metered_Utilization)
- [Clone Metered Utilization](https://docs.oracle.com/en-us/iaas/Content/File/Concepts/FSutilization.htm#FSutilization_topic-clone_metered_utilization)
- [Replication Metered Utilization](https://docs.oracle.com/en-us/iaas/Content/File/Concepts/FSutilization.htm#FSutilization_topic-replication_metered_utilization)
- [Metadata Metered Utilization](https://docs.oracle.com/en-us/iaas/Content/File/Concepts/FSutilization.htm#Metadata_Metered_Utilization)
- [Using DF and DU Commands](https://docs.oracle.com/en-us/iaas/Content/File/Concepts/FSutilization.htm#Using)
- [Mount Target Performance](https://docs.oracle.com/en-us/iaas/Content/File/Tasks/managingmounttargets.htm#performance)

To validate the details you can
1) check Utilization in OCI UI 				(File Storage - File Systems - File System details)
2) check Snapshots in OCI UI 				(File Storage - File Systems - File System details - Snapshots)
3) check Replications in OCI UI 			(File Storage - File Systems - File System details - Replications)
4) check Clones in OCI UI 					(File Storage - File Systems - File System details - Clones)
5) check Metric File System Usage in OCI UI (File Storage - File Systems - File System details - Metrics; adjust Start/End time)
6) check Mount Target throughput in OCI UI	(File Storage - Mount Targets - Mount Target details)




## Summary

Please use this script as a template for your individual and automated OCI Storage Health-Check.



<!--- Links -->

[home]:                        https://gitlab.com/hmielimo/next-generation-cloud

<!-- /Links -->
